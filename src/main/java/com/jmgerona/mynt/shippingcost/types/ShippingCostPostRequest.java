package com.jmgerona.mynt.shippingcost.types;

import lombok.Data;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;

@Data
public class ShippingCostPostRequest {
    @NotNull
    @Positive
    private Float weight;

    @NotNull
    @Positive
    private Float height;

    @NotNull
    @Positive
    private Float width;

    @NotNull
    @Positive
    private Float length;

    private String voucherCode;
}

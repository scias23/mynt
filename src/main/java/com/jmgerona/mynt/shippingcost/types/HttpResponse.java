package com.jmgerona.mynt.shippingcost.types;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public abstract class HttpResponse {
    @JsonProperty
    private String referenceId;
}

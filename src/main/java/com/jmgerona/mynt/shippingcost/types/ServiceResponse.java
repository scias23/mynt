package com.jmgerona.mynt.shippingcost.types;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ServiceResponse {
    private Float volume;
    private Float weight;
    private Float cost;
    private String kind;
    private String voucherCode;
}

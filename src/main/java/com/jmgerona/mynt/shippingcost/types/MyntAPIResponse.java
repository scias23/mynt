package com.jmgerona.mynt.shippingcost.types;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class MyntAPIResponse {
    private String code;
    private Float discount;
    private Date expiry;
}

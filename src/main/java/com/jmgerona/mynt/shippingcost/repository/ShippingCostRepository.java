package com.jmgerona.mynt.shippingcost.repository;

import com.jmgerona.mynt.shippingcost.model.ShippingCostRules;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ShippingCostRepository extends JpaRepository<ShippingCostRules, Long> {
}

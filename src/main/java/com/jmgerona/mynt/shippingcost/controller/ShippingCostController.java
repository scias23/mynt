package com.jmgerona.mynt.shippingcost.controller;

import java.util.UUID;

import com.jmgerona.mynt.shippingcost.service.ShippingCostService;
import com.jmgerona.mynt.shippingcost.types.ShippingCostPostRequest;
import com.jmgerona.mynt.shippingcost.types.ShippingCostPostResponse;
import com.jmgerona.mynt.shippingcost.types.ServiceResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

import static com.jmgerona.mynt.shippingcost.util.StringUtils.objectToString;

@RestController
@RequestMapping("cost")
public class ShippingCostController {
    private static final Logger LOGGER = LoggerFactory.getLogger(ShippingCostController.class);

    private ShippingCostService shippingCostService;

    @Autowired
    public void setShippingCostService(ShippingCostService shippingCostService) {
        this.shippingCostService = shippingCostService;
    }

    @PostMapping
    public ResponseEntity<ShippingCostPostResponse> post(
            @Valid @RequestBody ShippingCostPostRequest request) throws Exception {
        String referenceId = UUID.randomUUID().toString();
        MDC.put("referenceId", referenceId);

        LOGGER.info("REQUEST PAYLOAD {} ", objectToString(request));
        LOGGER.info("Computing shipping cost");

        ServiceResponse result = shippingCostService.computeShippingCost(request);

        ShippingCostPostResponse response = new ShippingCostPostResponse(
                result.getKind(),
                result.getWeight(),
                result.getVolume(),
                result.getCost(),
                result.getVoucherCode()
        );
        response.setReferenceId(referenceId);

        LOGGER.info("RESPONSE PAYLOAD {} ", objectToString(response));

        MDC.remove("referenceId");

        return new ResponseEntity<>(response, HttpStatus.OK);
    }
}

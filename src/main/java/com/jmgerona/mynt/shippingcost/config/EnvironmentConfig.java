package com.jmgerona.mynt.shippingcost.config;

import lombok.Data;
import org.springframework.stereotype.Component;

@Component
@Data
public class EnvironmentConfig {
    private String myntAPIUrl;

    private String myntAPIKey;

    public EnvironmentConfig() {
        this.myntAPIUrl = System.getenv("MYNT_API_URL");
        this.myntAPIKey =  System.getenv("MYNT_API_KEY");
    }
}

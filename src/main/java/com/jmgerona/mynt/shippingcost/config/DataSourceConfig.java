package com.jmgerona.mynt.shippingcost.config;

import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.sql.DataSource;

@Configuration
public class DataSourceConfig {
    @Bean
    public DataSource dataSource(){
        DataSourceBuilder datasource = DataSourceBuilder.create();
        datasource.url("jdbc:postgresql://" + System.getenv("DB_HOST") + "/" + System.getenv("DB_NAME"));
        datasource.username(System.getenv("DB_USER"));
        datasource.password(System.getenv("DB_PASSWORD"));
        return datasource.build();
    }
}

package com.jmgerona.mynt.shippingcost.external;

import com.jmgerona.mynt.shippingcost.config.EnvironmentConfig;
import com.jmgerona.mynt.shippingcost.types.MyntAPIResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

@Component
public class MyntAPI {
    private static final Logger LOGGER = LoggerFactory.getLogger(MyntAPI.class);

    private EnvironmentConfig environmentConfig;

    private RestTemplate restTemplate;

    @Autowired
    public MyntAPI(EnvironmentConfig environmentConfig, RestTemplate restTemplate) {
        this.environmentConfig = environmentConfig;
        this.restTemplate = restTemplate;
    }

    public MyntAPIResponse getVoucher(String voucher) {
        try {
            LOGGER.info("Getting voucher info from Mynt...");
            UriComponentsBuilder uri = UriComponentsBuilder.fromHttpUrl(environmentConfig.getMyntAPIUrl())
                    .path("/voucher/")
                    .path(voucher)
                    .queryParam("key", environmentConfig.getMyntAPIKey());
            MyntAPIResponse response = restTemplate.getForObject(uri.toUriString(), MyntAPIResponse.class);
            if (response == null) throw new RestClientException("");
            return response;
        } catch(RestClientException e) {
            LOGGER.error("Cannot find voucher. Won't apply any discount");
            return null;
        }
    }
}

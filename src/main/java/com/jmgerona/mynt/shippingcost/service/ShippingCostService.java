package com.jmgerona.mynt.shippingcost.service;

import com.jmgerona.mynt.shippingcost.external.MyntAPI;
import com.jmgerona.mynt.shippingcost.model.ShippingCostRules;
import com.jmgerona.mynt.shippingcost.repository.ShippingCostRepository;
import com.jmgerona.mynt.shippingcost.types.MyntAPIResponse;
import com.jmgerona.mynt.shippingcost.types.ServiceResponse;
import com.jmgerona.mynt.shippingcost.types.ShippingCostPostRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

import static com.jmgerona.mynt.shippingcost.util.StringUtils.objectToString;

@Service
public class ShippingCostService {
    private static final Logger LOGGER = LoggerFactory.getLogger(ShippingCostService.class);

    private ShippingCostRepository repository;

    private MyntAPI myntAPI;

    @Autowired
    public void setShippingCostRepository(ShippingCostRepository repository) {
        this.repository = repository;
    }

    @Autowired
    public void setMyntAPI(MyntAPI myntAPI) { this.myntAPI = myntAPI; }

    public ServiceResponse computeShippingCost(ShippingCostPostRequest request) throws Exception {
        // get rules from database
        List<ShippingCostRules> rules = repository.findAll(Sort.by(Sort.Direction.ASC, "priority"));
        ShippingCostRules matchedRule = rules.stream().filter(rule -> evaluate(request, rule)).findFirst().orElse(null);

        // find voucher if supplied in request
        MyntAPIResponse myntVoucher = null;
        if (request.getVoucherCode() != null) {
            myntVoucher = myntAPI.getVoucher(request.getVoucherCode());
            if (myntVoucher != null) {
                LOGGER.info("Voucher code result from MYNT {} ", objectToString(myntVoucher));
                myntVoucher = isValidVoucher(myntVoucher);
            }
        }

        ServiceResponse response = new ServiceResponse(
                calculateVolume(request),
                request.getWeight(),
                calculateCost(request, matchedRule, myntVoucher),
                matchedRule.getRuleName(),
                myntVoucher != null ? myntVoucher.getCode() : null);
        LOGGER.info("Cost service calculated cost {}", objectToString(response));

        return response;
    }

    private boolean evaluate(ShippingCostPostRequest request, ShippingCostRules rule) {
        if (rule.getThreshold() == null) return true;
        float volume = calculateVolume(request);
        float threshold = rule.getThreshold();

        switch(rule.getUom()) {
            case "weight": if(threshold < request.getWeight()) return true; break;
            case "volume": if(threshold > volume) return true; break;
            default: return false;
        }
        return false;
    }

    private Float calculateCost(ShippingCostPostRequest request, ShippingCostRules rule, MyntAPIResponse voucher) {
        Float cost = rule.getCost();
        if (cost == null) return null;
        float volume = calculateVolume(request);

        Float finalCost;
        switch(rule.getUom()) {
            case "weight": finalCost = cost * request.getWeight(); break;
            case "volume": finalCost = cost * volume; break;
            default: finalCost = null;
        }
        if(voucher == null) return finalCost;
        return finalCost - voucher.getDiscount();
    }

    private float calculateVolume(ShippingCostPostRequest request) {
        return request.getHeight() * request.getWidth() * request.getLength();
    }

    private MyntAPIResponse isValidVoucher(MyntAPIResponse voucher) {
        return voucher.getExpiry().after(new Date()) ? voucher : null;
    }
}

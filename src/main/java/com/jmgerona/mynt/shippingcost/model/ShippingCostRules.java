package com.jmgerona.mynt.shippingcost.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Null;

@Entity
@Table(name = "shipping_cost_rules")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class ShippingCostRules {
    @Id
    private Integer priority;

    @Column(name = "rule_name")
    @NotNull
    private String ruleName;

    @Column(name = "threshold")
    @Null
    private Float threshold;

    @Column(name = "uom")
    @Null
    private String uom;

    @Column(name = "cost")
    @Null
    private Float cost;
}

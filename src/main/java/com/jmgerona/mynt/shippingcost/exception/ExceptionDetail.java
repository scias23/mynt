package com.jmgerona.mynt.shippingcost.exception;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ExceptionDetail {
    @JsonProperty
    private int status;

    @JsonProperty
    private String message;
}

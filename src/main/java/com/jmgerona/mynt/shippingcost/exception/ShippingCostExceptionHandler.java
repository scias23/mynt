package com.jmgerona.mynt.shippingcost.exception;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
public class ShippingCostExceptionHandler {
    private static final Logger LOGGER = LoggerFactory.getLogger(ShippingCostExceptionHandler.class);

    @ExceptionHandler({
        HttpRequestMethodNotSupportedException.class,
        MethodArgumentNotValidException.class,
        HttpMessageNotReadableException.class
    })
    @ResponseStatus(value = HttpStatus.BAD_REQUEST)
    private ExceptionDetail badRequest() {
        return new ExceptionDetail(400, "Bad request");
    }

    @ExceptionHandler(Exception.class)
    @ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR)
    public ExceptionDetail exception(Exception e) {
        if (LOGGER.isErrorEnabled()) {
            LOGGER.error(e.getMessage());
        }
        return new ExceptionDetail(500, "Internal error");
    }

}

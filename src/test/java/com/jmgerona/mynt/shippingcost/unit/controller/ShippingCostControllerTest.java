package com.jmgerona.mynt.shippingcost.unit.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.jmgerona.mynt.shippingcost.Application;
import com.jmgerona.mynt.shippingcost.controller.ShippingCostController;
import com.jmgerona.mynt.shippingcost.exception.ExceptionDetail;
import com.jmgerona.mynt.shippingcost.external.MyntAPI;
import com.jmgerona.mynt.shippingcost.model.ShippingCostRules;
import com.jmgerona.mynt.shippingcost.repository.ShippingCostRepository;
import com.jmgerona.mynt.shippingcost.types.MyntAPIResponse;
import com.jmgerona.mynt.shippingcost.types.ShippingCostPostRequest;
import com.jmgerona.mynt.shippingcost.types.ShippingCostPostResponse;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.Sort;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.web.servlet.MockMvc;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;

@WebMvcTest(ShippingCostController.class)
@ContextConfiguration(classes={Application.class})
@ExtendWith(MockitoExtension.class)
public class ShippingCostControllerTest {

    @MockBean
    private ShippingCostRepository repository;

    @MockBean
    private MyntAPI myntAPI;

    @Autowired
    private MockMvc mockMvc;

    ObjectMapper mapper = new ObjectMapper();

    @BeforeEach
    public void init() {
        List<ShippingCostRules> testRules = new ArrayList<>();
        testRules.add(new ShippingCostRules(1, "Reject", (float) 50, "weight", null));
        testRules.add(new ShippingCostRules(2, "Heavy Parcel", (float) 10, "weight", (float) 20));
        testRules.add(new ShippingCostRules(3, "Small Parcel", (float) 1500, "volume", (float) 0.03));
        testRules.add(new ShippingCostRules(4, "Medium Parcel", (float) 2500, "volume", (float) 0.04));
        testRules.add(new ShippingCostRules(5, "Large Parcel", null, "volume", (float) 0.05));
        when(repository.findAll(any(Sort.class))).thenReturn(testRules);

        Calendar c = Calendar.getInstance();
        c.setTime(new Date());
        c.add(Calendar.DATE, 1);

        String voucherCode = "PLEASE_HIRE_ME";
        // THANK YOU IN ADVANCE IF YOU GUYS DECIDE TO. :)
        when(myntAPI.getVoucher(any(String.class))).thenReturn(new MyntAPIResponse(voucherCode, (float) 50, c.getTime()));
    }

    @AfterEach
    public void teardown() {
        Mockito.reset(repository);
        Mockito.reset(myntAPI);
    }

    // testing sample regular shipping cost
    @Test
    public void givenNoVoucherCodeProvided_whenRequestingCost_thenShouldReceiveRegularCostResponse() throws Exception {
        ShippingCostPostRequest request = new ShippingCostPostRequest();
        request.setWeight((float) 5);
        request.setHeight((float) 10);
        request.setWidth((float) 10);
        request.setLength((float) 10);

        ShippingCostPostResponse response = mapper.readValue(getPostResponse(request), ShippingCostPostResponse.class);

        assertThat(response).hasFieldOrPropertyWithValue("kind", "Small Parcel");
        assertThat(response).hasFieldOrPropertyWithValue("weight", (float) 5.0);
        assertThat(response).hasFieldOrPropertyWithValue("volume", (float) 1000.0);
        assertThat(response).hasFieldOrPropertyWithValue("cost", (float) 30.0);
    }

    // testing sample discounted shipping cost
    @Test
    public void givenValidVoucherCodeProvided_whenRequestingCost_thenShouldReceiveDiscountedCostResponse() throws Exception {
        ShippingCostPostRequest request = new ShippingCostPostRequest();
        request.setWeight((float) 15);
        request.setHeight((float) 15);
        request.setWidth((float) 15);
        request.setLength((float) 15);
        request.setVoucherCode("PLEASE_HIRE_ME");

        ShippingCostPostResponse response = mapper.readValue(getPostResponse(request), ShippingCostPostResponse.class);

        assertThat(response).hasFieldOrPropertyWithValue("kind", "Heavy Parcel");
        assertThat(response).hasFieldOrPropertyWithValue("weight", (float) 15.0);
        assertThat(response).hasFieldOrPropertyWithValue("volume", (float) 3375.0);
        assertThat(response).hasFieldOrPropertyWithValue("cost", (float) 250.0);
    }

    // testing bad request
    @Test
    public void givenIncompleteInput_whenRequestingCost_thenShouldReceiveErrorResponse() throws Exception {
        ShippingCostPostRequest request = new ShippingCostPostRequest();
        request.setWeight((float) 15);
        request.setLength((float) 30);

        ExceptionDetail response = mapper.readValue(getPostResponse(request), ExceptionDetail.class);

        assertThat(response).hasFieldOrPropertyWithValue("status", 400);
        assertThat(response).hasFieldOrPropertyWithValue("message", "Bad request");
    }

    // regular cost : all cases

    // priority 1: reject
    @Test
    public void givenInputForRejectParcel_whenRequestingCost_thenReturnRejectParcel() throws Exception {
        ShippingCostPostRequest request = new ShippingCostPostRequest();
        request.setWeight((float) 51);
        request.setHeight((float) 25);
        request.setWidth((float) 25);
        request.setLength((float) 10);

        ShippingCostPostResponse response = mapper.readValue(getPostResponse(request), ShippingCostPostResponse.class);

        assertThat(response).hasFieldOrPropertyWithValue("kind", "Reject");
        assertThat(response).hasFieldOrPropertyWithValue("weight", (float) 51.0);
        assertThat(response).hasFieldOrPropertyWithValue("volume", (float) 6250.0);
    }

    // priority 2: heavy parcel
    @Test
    public void givenInputForHeavyParcel_whenRequestingCost_thenReturnHeavyParcelCost() throws Exception {
        ShippingCostPostRequest request = new ShippingCostPostRequest();
        request.setWeight((float) 15);
        request.setHeight((float) 10);
        request.setWidth((float) 25);
        request.setLength((float) 15);

        ShippingCostPostResponse response = mapper.readValue(getPostResponse(request), ShippingCostPostResponse.class);

        assertThat(response).hasFieldOrPropertyWithValue("kind", "Heavy Parcel");
        assertThat(response).hasFieldOrPropertyWithValue("weight", (float) 15.0);
        assertThat(response).hasFieldOrPropertyWithValue("volume", (float) 3750.0);
        assertThat(response).hasFieldOrPropertyWithValue("cost", (float) 300.0);
    }

    // priority 3: small parcel
    @Test
    public void givenInputForSmallParcel_whenRequestingCost_thenReturnSmallParcelCost() throws Exception {
        ShippingCostPostRequest request = new ShippingCostPostRequest();
        request.setWeight((float) 8);
        request.setHeight((float) 5);
        request.setWidth((float) 5);
        request.setLength((float) 10);

        ShippingCostPostResponse response = mapper.readValue(getPostResponse(request), ShippingCostPostResponse.class);

        assertThat(response).hasFieldOrPropertyWithValue("kind", "Small Parcel");
        assertThat(response).hasFieldOrPropertyWithValue("weight", (float) 8.0);
        assertThat(response).hasFieldOrPropertyWithValue("volume", (float) 250.0);
        assertThat(response).hasFieldOrPropertyWithValue("cost", (float) 7.5);
    }

    // priority 4: medium parcel
    @Test
    public void givenInputForMediumParcel_whenRequestingCost_thenReturnMediumParcelCost() throws Exception {
        ShippingCostPostRequest request = new ShippingCostPostRequest();
        request.setWeight((float) 8);
        request.setHeight((float) 25);
        request.setWidth((float) 8);
        request.setLength((float) 10);

        ShippingCostPostResponse response = mapper.readValue(getPostResponse(request), ShippingCostPostResponse.class);

        assertThat(response).hasFieldOrPropertyWithValue("kind", "Medium Parcel");
        assertThat(response).hasFieldOrPropertyWithValue("weight", (float) 8.0);
        assertThat(response).hasFieldOrPropertyWithValue("volume", (float) 2000.0);
        assertThat(response).hasFieldOrPropertyWithValue("cost", (float) 80.0);
    }

    // priority 5: large parcel
    @Test
    public void givenInputForLargeParcel_whenRequestingCost_thenReturnLargeParcelCost() throws Exception {
        ShippingCostPostRequest request = new ShippingCostPostRequest();
        request.setWeight((float) 7);
        request.setHeight((float) 20);
        request.setWidth((float) 25);
        request.setLength((float) 10);

        ShippingCostPostResponse response = mapper.readValue(getPostResponse(request), ShippingCostPostResponse.class);

        assertThat(response).hasFieldOrPropertyWithValue("kind", "Large Parcel");
        assertThat(response).hasFieldOrPropertyWithValue("weight", (float) 7.0);
        assertThat(response).hasFieldOrPropertyWithValue("volume", (float) 5000.0);
        assertThat(response).hasFieldOrPropertyWithValue("cost", (float) 250.0);
    }

    private String getPostResponse(ShippingCostPostRequest request) throws Exception {
        return mockMvc.perform(post("/cost")
                .content(mapper.writeValueAsString(request))
                .contentType(MediaType.APPLICATION_JSON))
                .andReturn().getResponse().getContentAsString();
    }
}

package com.jmgerona.mynt.shippingcost.system;

import com.jmgerona.mynt.shippingcost.Application;
import com.jmgerona.mynt.shippingcost.exception.ExceptionDetail;
import com.jmgerona.mynt.shippingcost.types.ShippingCostPostRequest;
import com.jmgerona.mynt.shippingcost.types.ShippingCostPostResponse;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import static org.assertj.core.api.Assertions.assertThat;

// These test cases are designed to run against an actual live endpoint.
// URL can be changed per pipeline stage by setting the environment variable SHIPPING_COST_API_URL
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureMockMvc
@ContextConfiguration(classes={Application.class})
@RunWith(SpringRunner.class)
public class ShippingCostSystemTest {
    @Autowired
    private TestRestTemplate testRestTemplate;

    private String BASE_URL = System.getenv("SHIPPING_COST_API_URL");

    @Test
    public void givenNoVoucherCodeProvided_whenRequestingCost_thenShouldReceiveResponse() throws Exception {
        ShippingCostPostRequest request = new ShippingCostPostRequest();
        request.setWeight((float) 5);
        request.setHeight((float) 10);
        request.setWidth((float) 10);
        request.setLength((float) 10);

        ShippingCostPostResponse response =
                testRestTemplate.postForObject(BASE_URL + "/cost", request, ShippingCostPostResponse.class);

        assertThat(response).hasFieldOrPropertyWithValue("kind", "Small Parcel");
        assertThat(response).hasFieldOrPropertyWithValue("weight", 5.0f);
        assertThat(response).hasFieldOrPropertyWithValue("volume", 1000.0f);
        assertThat(response).hasFieldOrPropertyWithValue("cost", 30.0f);
    }

    @Test
    public void givenExpiredVoucherCodeProvided_whenRequestingCost_thenShouldReceiveResponse() throws Exception {
        ShippingCostPostRequest request = new ShippingCostPostRequest();
        request.setWeight((float) 5);
        request.setHeight((float) 10);
        request.setWidth((float) 10);
        request.setLength((float) 10);
        request.setVoucherCode("MYNT");

        ShippingCostPostResponse response =
                testRestTemplate.postForObject(BASE_URL + "/cost", request, ShippingCostPostResponse.class);

        assertThat(response).hasFieldOrPropertyWithValue("kind", "Small Parcel");
        assertThat(response).hasFieldOrPropertyWithValue("weight", 5.0f);
        assertThat(response).hasFieldOrPropertyWithValue("volume", 1000.0f);
        assertThat(response).hasFieldOrPropertyWithValue("cost", 30.0f);
    }

    @Test
    public void givenIncompleteInput_whenRequestingCost_thenShouldErrorResponse() throws Exception {
        ShippingCostPostRequest request = new ShippingCostPostRequest();
        request.setWidth((float) 10);
        request.setLength((float) 10);
        request.setVoucherCode("MYNT");

        ExceptionDetail response =
                testRestTemplate.postForObject(BASE_URL + "/cost", request, ExceptionDetail.class);

        assertThat(response).hasFieldOrPropertyWithValue("status", 400);
        assertThat(response).hasFieldOrPropertyWithValue("message", "Bad request");
    }
}

#!/bin/bash

DB_STRING="postgres://$DB_USER:$DB_PASSWORD@$DB_HOST/$DB_NAME$DB_CONN_EXTRA_ARGS"

goose -dir /migration postgres $DB_STRING $1

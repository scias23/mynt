# jmgerona/mynt-practical-exam

A very small API that computes shipping cost based on user supplied parcel weight and volume information.

## Assumptions

* Discounts are interpreted as deductable pesos from the computed cost. For example, if cost before discount is `30`, 
  and if MYNT discount code is `12.25`, Php 12.25 pesos will be deducted from the total cost, which gives Php 17.75. 
* Validity date is being checked when requesting for voucher information from Mynt API. So in actual usage of this
API practical exam, no available vouchers can be applied as both `GFI` and `MYNT` voucher codes are already expired.

## JM's local environment
This small API was written using a Mac computer, with Docker and GNU Make installed.

## Shiny project features

- [x] Containerized local development
- [x] No need to install Java, Maven, Spring, or Postgres on your local machine. Simply pull this repository and
  run `make DOTENV=.env.example && make run` and you are all set!
- [x] CI/CD ready. An working sample CI/CD configuration is provided.
- [x] Uses an actual Postgres database. A working Postgres database will be automatically setup for you when running locally.
- [x] Database migrations enabled to demonstrate how easy to modify computation rules
- [x] Containerized deployment package. Immutable environment all from dev to production. (incomplete though due to time 
  constraints)

## Note on containerized development
The dependencies will be downloaded into the root folder of the project. So first time running `make DOTENV=.env.example && make run`
will take a while. Improvement can be done by using the root `.m2` from the users computer. But for the
purposes of making the project self contained, I decided to have the `.m2` folder on the project's root folder.

## Note on actual deployment via CI/CD

My target was to deploy the application to an
actual AWS endpoint but ran out of time. However, deployment can be easily added by adding in the 
`make` and `docker-compose.yml` the deployment targets

## Pre-requisites

Since this project setup is containerized, only these three are needed to install on your local.
This is made possible by applying the concepts detailed in [3musketeers.io](https://3musketeers.io/)

* Docker
* Docker Compose
* Make

## How to run locally

When pulling the repository the first time

```bash
git clone git@bitbucket.org:scias23/mynt.git
cd ./mynt
make DOTENV=.env.example && make run
```

Subsequent runs

```bash
make run
```

## Running tests

Unit tests and System/Integration tests are on their own Make targets. The reason is in a CI/CD pipeline setting,
we want the unit tests to execute first, then deploy the application, then run the system tests against the live endpoint.
Since there is a time constraint, I was not able to create a self contained integration test. But if in a pipeline setting,
integration tests can be its own stage.

### CI/CD Pipeline steps layout if with integration tests
1. `make deps` (get dependencies and zip as an artifact)
2. `make testUnit` (run unit tests)
3. `make testIntegration` (run integration tests)
4. `make deploy` (deploy application to a live environment) (I was not able to create actual deployment to AWS 
   elasticbeanstalk because of time constraints)
5. `make testSystem` (run test cases against the live endpoint)

Run the unit tests:

```bash
make testUnit
```

Run the system (or integration) tests:

```bash
make testSystem
```

Kindly see the `make testUnit` step of this [pipeline build to see a valid voucher code being mocked in order to show a response with valid voucher code applied.](https://bitbucket.org/scias23/mynt/addon/pipelines/home#!/results/35)

## Packaging application

```bash
make package
```

## Running database migrations

One step up

```bash
make migrateUp
```

One step down
```bash
make migrateDown
```

## Deploying the application

Since there is a time constraint and family time, deploy stage is incomplete. Also, I was not able to create an live 
Postgres database to host the database on the cloud. Can be done in AWS RDS though.

In practice, running deployment from your local or from CI/CD pipeline is:

```bash
make deploy
```

# Sample payload

This application only processes successfully on POST at `/cost` endpoint. Other HTTP methods are not supported.

## POST

`POST http://localhost:8080/cost`

### Request (with expired voucher)

```
{
    "weight": "50",
    "height": "20",
    "width": "12",
    "length": "22",
    "voucherCode": "GFI"
}
```

### Response (voucherCode is not applied)

```
{
    "referenceId": "fbcf425b-00f5-48dd-8ef6-1832f0fec4ea",
    "kind": "Heavy Parcel",
    "weight": 50.0,
    "volume": 5280.0,
    "cost": 1000.0
}
```

### Request (with valid voucher, for 10 php discount)

```
{
    "weight": "50",
    "height": "20",
    "width": "12",
    "length": "22",
    "voucherCode": "TESTVALIDVOUCHER"
}
```

### Response (voucherCode is applied)

```
{
    "referenceId": "fbcf425b-00f5-48dd-8ef6-1832f0fec4ea",
    "kind": "Heavy Parcel",
    "weight": 50.0,
    "volume": 5280.0,
    "cost": 990.0,
    "voucherCodeApplied": "TESTVALIDVOUCHER"
}
```

## GET

`GET http://localhost:8080/cost`

### Response

```
{
    "status": 400,
    "message": "Bad request"
}
```

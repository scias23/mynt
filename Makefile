# Use existing .env file if any, or overwrite by make DOTENV=new.env.file
ifdef DOTENV
	DOTENV_TARGET=dotenv
else
	DOTENV_TARGET=.env
endif

# Command aliases
mvnConfig := maven mvn -gs m2-settings.xml
mvn := docker compose run --rm -p 55555 $(mvnConfig)
make := docker compose run --rm make make
goose := docker compose run --rm goose

#################
# CI/CD targets #
#################

# Get project dependencies
deps: $(DOTENV_TARGET)
	@$(mvn) clean dependency:copy-dependencies

# Execute unit tests
testUnit: $(DOTENV_TARGET)
	@$(mvn) '-Dtest=com.jmgerona.mynt.shippingcost.unit.**' test

# Execute system tests
# meant to run against an actual live API endpoint
testSystem: $(DOTENV_TARGET)
	@$(mvn) '-Dtest=com.jmgerona.mynt.shippingcost.system.**' test

# Package application
package: $(DOTENV_TARGET)
	@$(mvn) clean package -DskipTests=true
	#docker build -t jmgerona/mynt-shipping-cost-calc-svc:latest .

# Zip dependencies to use as build artifact in CI/CD
.m2.zip: $(DOTENV_TARGET)
	@$(make) _.m2.zip
.PHONY: .m2.zip

migrateUp: $(DOTENV_TARGET)
	@$(goose) up

migrateDown: $(DOTENV_TARGET)
	@$(goose) down

# TODO: deploy steps incomplete. ran out of time
deploy: $(DOTENV_TARGET)
	docker compose run --rm elasticbeanstalk

# Unzip dependencies to reuse already downloaded dependencies from previous CI/CD stage
# Usage depends on CI/CD tool being used if there is no auto unzipping of downloaded artifacts
.m2.unzip: $(DOTENV_TARGET)
	@$(make) _.m2.unzip
.PHONY: .m2.unzip

#####################
# Local development #
#####################

# Stops all containers
stop:
	docker compose down --remove-orphans

# One line command to initialize local environment
run: $(DOTENV_TARGET) runPostgres migrateUp runSpring

# Run Spring boot application
runSpring: $(DOTENV_TARGET)
	docker compose run --rm --service-ports $(mvnConfig) spring-boot:run

# Run Postgres db
runPostgres: $(DOTENV_TARGET)
	docker compose up -d postgres
	sleep 10

##########
# Others #
##########

# Create .env based on .env.template if .env does not exist
.env:
	@echo "Create .env with .env.template"
	cp .env.template .env

# Create/Overwrite .env with $(DOTENV)
dotenv:
	@echo "Overwrite .env with $(DOTENV)"
	cp $(DOTENV) .env

_.m2.zip:
	zip -rq .m2.zip .m2/

_.m2.unzip:
	unzip -qo -d . .m2.zip

install: $(DOTENV_TARGET)
	@$(mvn) clean install -DskipTests=true

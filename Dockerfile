FROM adoptopenjdk:8-jre-hotspot as builder
ARG JAR_FILE=target/*.jar
COPY ${JAR_FILE} application.jar
RUN env > .env
COPY .env .env
RUN java -Djarmode=layertools -jar application.jar extract

FROM adoptopenjdk:8-jre-hotspot
RUN source .env
COPY --from=builder dependencies/ ./
COPY --from=builder snapshot-dependencies/ ./
COPY --from=builder spring-boot-loader/ ./
COPY --from=builder application/ ./
ENTRYPOINT ["java", "org.springframework.boot.loader.JarLauncher"]

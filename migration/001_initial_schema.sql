-- +goose Up
-- +goose StatementBegin
CREATE TABLE shipping_cost_rules
(
    priority int NOT NULL,
    rule_name varchar(20) NOT NULL,
    threshold float(2),
    uom varchar(10) NOT NULL,
    cost float(2),
    PRIMARY KEY (priority)
);

INSERT INTO shipping_cost_rules
VALUES
    ('1','Reject',50,'weight',null),
    ('2','Heavy Parcel',10,'weight',20),
    ('3','Small Parcel',1500,'volume',0.03),
    ('4','Medium Parcel',2500,'volume',0.04),
    ('5','Large Parcel',null,'volume',0.05);
-- +goose StatementEnd

-- +goose Down
DROP TABLE IF EXISTS delivery_cost_rules;
